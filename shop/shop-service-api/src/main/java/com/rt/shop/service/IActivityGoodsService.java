package com.rt.shop.service;

import com.baomidou.framework.service.ISuperService;
import com.rt.shop.entity.ActivityGoods;

/**
 *
 * ActivityGoods 表数据服务层接口
 *
 */
public interface IActivityGoodsService extends ISuperService<ActivityGoods> {



}